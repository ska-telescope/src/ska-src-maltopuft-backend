"""External catalogue service request schemas."""

from typing import Annotated

from fastapi import Query
from pydantic import Field

from ska_src_maltopuft_backend.core.custom_types import PositiveList
from ska_src_maltopuft_backend.core.schemas import (
    CommonQueryParams,
    RaDecPositionQueryParameters,
)


class GetKnownPulsarQueryParams(
    CommonQueryParams,
    RaDecPositionQueryParameters,
):
    """Query parameters for KnownPulsar model HTTP GET requests."""

    name: list[str | None] = Field(Query(default=[]))
    dm: Annotated[PositiveList[float], None] = Field(Query(default=[]))
    width: Annotated[PositiveList[float], None] = Field(Query(default=[]))
    period: Annotated[PositiveList[float], None] = Field(Query(default=[]))
