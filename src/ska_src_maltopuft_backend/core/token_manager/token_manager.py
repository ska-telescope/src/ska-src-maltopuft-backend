"""Manages client credential access tokens."""

import logging
import time
from json import JSONDecodeError

import httpx
from pydantic import ValidationError

from ska_src_maltopuft_backend.core.config import settings
from ska_src_maltopuft_backend.core.exceptions import (
    InvalidClientTokenError,
    TokenProviderResponseError,
)

from .schemas import ClientAccessToken

logger = logging.getLogger(__name__)


class TokenManager:
    """Manages client credential access tokens.

    Example:
        >>> token_manager = TokenManager(
        ...     token_url="https://example.com/token",
        ...     client_id="your-client-id",
        ...     client_secret="your-client-secret",
        ...     audience="your-audience",
        ... )
        >>> token_manager.get_token()
        your-client-access-token

    """

    def __init__(
        self,
        token_url: str,
        client_id: str,
        client_secret: str,
        audience: str,
    ) -> None:
        """Initialises the TokenManager instance.

        Args:
            token_url (str): API endpoint for client token retrieval.
            client_id (str): IAM client UUID.
            client_secret (str): IAM client secret.
            audience (str): IAM client audience.

        """
        self.token_url: str = token_url
        self.client_id: str = client_id
        self.client_secret: str = client_secret
        self.audience: str = audience
        self.token: str | None = None
        self.token_expires_at: int = 0

    async def get_token(self) -> str | None:
        """Gets the base64 encoded client access token.

        Returns
            str | None: Access token

        """
        if settings.AUTH_DISABLED:
            return None

        current_time = int(time.time())
        if self.token is None or current_time >= self.token_expires_at:
            await self._refresh_token()
        return self.token

    async def prepare_auth_header(self) -> dict[str, str]:
        """Returns a dictionary with the Authorization header.

        Returns
            dict[str, str]: Authorization header.

        """
        token = await self.get_token()
        if token is None:
            return {}
        return {"Authorization": f"Bearer {token}"}

    async def _refresh_token(self) -> None:
        """Fetches and sets an access token for this instance."""
        async with httpx.AsyncClient() as client:
            response = await client.post(
                self.token_url,
                data={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "audience": self.audience,
                    "grant_type": "client_credentials",
                },
            )

        try:
            response.raise_for_status()
        except httpx.HTTPStatusError as e:
            self.token = None
            self.token_expires_at = 0

            raise TokenProviderResponseError(
                message=(
                    f"Error retrieving service client token: {e.response.text}"
                ),
                status_code=e.response.status_code,
            ) from e

        try:
            token_provider_response = response.json()
            token = ClientAccessToken(**token_provider_response)
        except (JSONDecodeError, ValidationError) as e:
            self.token = None
            self.token_expires_at = 0
            raise InvalidClientTokenError from e

        self.token = token.access_token
        self.token_expires_at = int(time.time()) + token.expires_in


token_manager = TokenManager(
    token_url=f"{settings.AUTH.MALTOPUFT_IAM_SERVER}/token",
    client_id=settings.AUTH.MALTOPUFT_IAM_CLIENT_ID,
    client_secret=settings.AUTH.MALTOPUFT_IAM_CLIENT_SECRET,
    audience=settings.AUTH.MALTOPUFT_IAM_CLIENT_NAME,
)


def get_token_manager() -> TokenManager:
    """TokenManager dependency.

    Returns:
        (TokenManager): TokenManager instance.

    Example:
        >>> from typing import Annotated
        >>> from fastapi import APIRouter, Depends
        >>> from ska_src_maltopuft_backend.core.token_manager import (
        ...     TokenManager, get_token_manager
        ... )
        >>> my_router = APIRouter()
        >>> @my_router.get("/")
        >>> async def my_endpoint(
        ...     token_manager: Annotated[
        ...         TokenManager,
        ...         Depends(get_token_manager)
        ...     ],
        ... ):
        >>>     token = await token_manager.get_token()
        >>>     print(token)
        your-client-access-token

    """
    return token_manager
