"""Token manager schemas."""

from pydantic import BaseModel, PositiveInt


class ClientAccessToken(BaseModel):
    """Client credentials access token schema."""

    access_token: str
    token_type: str
    expires_in: PositiveInt
    scope: str
