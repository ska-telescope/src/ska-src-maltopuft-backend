"""Create a FastAPI application."""

from collections.abc import Callable

from fastapi import FastAPI, Request
from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, JSONResponse
from pyinstrument import Profiler
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.sessions import SessionMiddleware

from ska_src_maltopuft_backend.app.api import router
from ska_src_maltopuft_backend.core.auth import BearerTokenAuthBackend
from ska_src_maltopuft_backend.core.config import settings
from ska_src_maltopuft_backend.core.exceptions import MaltopuftError

from .database.database import get_db


def init_routers(app_: FastAPI) -> None:
    """Include all routers defined in src.api initialisation."""
    app_.include_router(router)


def init_listeners(app_: FastAPI) -> None:
    """Register Fast API app listeners."""

    @app_.exception_handler(MaltopuftError)
    async def custom_exception_handler(
        request: Request,  # pylint: disable=W0613 # noqa: ARG001
        exc: MaltopuftError,
    ) -> JSONResponse:
        return JSONResponse(
            status_code=exc.status_code,
            content={
                "message": exc.message,
                "status_code": exc.status_code,
            },
        )


def register_profiling_middleware(app_: FastAPI) -> None:
    """Register a middleware to profile requests.

    Add a query parameter `profile=1` to any request to enable profiling.

    Args:
        app_ (FastAPI): FastAPI application instance

    """

    @app_.middleware("http")
    async def profile_request(
        request: Request,
        call_next: Callable,
    ) -> HTMLResponse | Callable:
        profiling = request.query_params.get("profile", False)
        if profiling:
            profiler = Profiler()
            profiler.start()
            await call_next(request)
            profiler.stop()
            return HTMLResponse(profiler.output_html())

        return await call_next(request)


def make_middleware() -> list[Middleware]:
    """Return an ordered list of "middleware" used by the Fast API
    application.

    Ordered means that earlier list elements take priority over later list
    elements.
    """
    return [
        Middleware(
            SessionMiddleware,
            secret_key=settings.AUTH.MALTOPUFT_IAM_AUTHORIZATION_STATE,
        ),
        Middleware(
            CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
            expose_headers=["*"],
        ),
        Middleware(
            AuthenticationMiddleware,
            backend=BearerTokenAuthBackend(db=next(get_db())),
            on_error=BearerTokenAuthBackend.on_auth_error,
        ),
    ]


def create_app() -> FastAPI:
    """Create a Fast API application."""
    app_ = FastAPI(middleware=make_middleware())
    register_profiling_middleware(app_)
    init_routers(app_=app_)
    init_listeners(app_=app_)
    return app_


app = create_app()
