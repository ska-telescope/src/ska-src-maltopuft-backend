"""Utils for SQLAlchemy models."""

from typing import Any

from sqlalchemy import Row, inspect

from ska_src_maltopuft_backend.core.custom_types import ModelT


def as_dict(obj: Row[ModelT]) -> dict[str, Any]:
    """Converts a SQLAlchemy model object to a dictionary.

    Only includes column attributes, ignoring relationships.

    Args:
        obj: SQLAlchemy model instance to convert

    Returns:
        Dictionary containing only column attributes

    """
    return {
        c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs
    }
