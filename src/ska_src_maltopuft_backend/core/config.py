"""Application configuration."""

from pydantic import PostgresDsn, computed_field
from pydantic_settings import BaseSettings, SettingsConfigDict


class AuthSettings(BaseSettings):
    """Configures settings used for authentication."""

    model_config = SettingsConfigDict(
        env_file=".env",
        # Allow environment variables not explicitly defined in the settings
        # class to be loaded.
        extra="ignore",
    )

    AUTHN_API_URL: str = ""

    # Service account settings
    MALTOPUFT_IAM_SERVER: str = ""
    MALTOPUFT_IAM_CLIENT_NAME: str = ""
    MALTOPUFT_IAM_CLIENT_ID: str = ""
    MALTOPUFT_IAM_CLIENT_SECRET: str = ""
    MALTOPUFT_IAM_CLIENT_SCOPES: str = ""
    MALTOPUFT_IAM_AUTHORIZATION_STATE: str = ""
    MALTOPUFT_IAM_SERVICE_GROUP: str = ""
    MALTOPUFT_IAM_ROOT_GROUP: str = ""


class Settings(BaseSettings):
    """Configures settings used throughout the application."""

    # pylint: disable=C0103

    model_config = SettingsConfigDict(
        env_file=".env",
        # Allow environment variables not explicitly defined in the settings
        # class to be loaded.
        extra="ignore",
    )

    # Application settings
    APP_NAME: str = "ska-src-maltopuft-backend"
    RELEASE_VERSION: str = "0.1.0"
    DEBUG: int = 0
    AUTH_DISABLED: bool

    AUTH: AuthSettings = AuthSettings()

    # Database settings
    MALTOPUFT_POSTGRES_USER: str
    MALTOPUFT_POSTGRES_PASSWORD: str
    MALTOPUFT_POSTGRES_HOST: str
    MALTOPUFT_POSTGRES_PORT: int
    MALTOPUFT_POSTGRES_DB: str

    @computed_field  # type: ignore[misc]
    @property
    def MALTOPUFT_POSTGRES_INFO(self) -> str:  # noqa: N802
        """Database connection info without credentials."""
        return (
            f"postgresql+psycopg://{self.MALTOPUFT_POSTGRES_HOST}:"
            f"{self.MALTOPUFT_POSTGRES_PORT}/{self.MALTOPUFT_POSTGRES_DB}"
        )

    @computed_field  # type: ignore[misc]
    @property
    def MALTOPUFT_POSTGRES_URI(self) -> PostgresDsn:  # noqa: N802
        """Build the database connection string from settings."""
        return PostgresDsn.build(
            scheme="postgresql+psycopg",
            username=self.MALTOPUFT_POSTGRES_USER,
            password=self.MALTOPUFT_POSTGRES_PASSWORD,
            host=self.MALTOPUFT_POSTGRES_HOST,
            port=self.MALTOPUFT_POSTGRES_PORT,
            path=self.MALTOPUFT_POSTGRES_DB,
        )

    MALTOPUFT_ENTITIES: list[dict[str, str]] | None = None


settings: Settings = Settings()
