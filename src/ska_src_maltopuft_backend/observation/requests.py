"""Observation service request schemas."""

import datetime as dt
from typing import Annotated

from fastapi import Query
from pydantic import Field, PastDatetime

from ska_src_maltopuft_backend.core.custom_types import PositiveList
from ska_src_maltopuft_backend.core.schemas import CommonQueryParams


class GetObservationQueryParams(CommonQueryParams):
    """Query parameters for Observation model HTTP GET requests."""

    t_min: PastDatetime | None = None
    t_max: dt.datetime | None = None
    sp_candidate_id: Annotated[PositiveList[int], None] = Field(
        Query(default=[]),
    )
