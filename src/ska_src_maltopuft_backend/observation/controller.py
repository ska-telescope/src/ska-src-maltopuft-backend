"""Data controller for the Observation metadata models."""

from typing import Any

from sqlalchemy.orm import Session

from ska_src_maltopuft_backend.app.schemas.requests import (
    GetKnownPulsarQueryParams,
)
from ska_src_maltopuft_backend.catalogue.controller import (
    KnownPulsarController,
)
from ska_src_maltopuft_backend.core.controller import BaseController
from ska_src_maltopuft_backend.core.database.utils import as_dict

from .models import Observation
from .repository import ObservationRepository


class ObservationController(BaseController[Observation, None, None]):
    """Data controller for the Observation model."""

    def __init__(
        self,
        repository: ObservationRepository,
        known_pulsar_controller: KnownPulsarController,
    ) -> None:
        """Initalise a ObservationController instance."""
        super().__init__(
            model=Observation,
            repository=repository,
        )
        self.repository = repository
        self.pulsar_controller = known_pulsar_controller

    async def get_sources_by_ids(
        self,
        db: Session,
        ids_: list[int],
        radius: float,
    ) -> list[dict[str, Any | list[dict[str, Any]]]]:
        """Returns known sources within a radius of list of observation ids."""
        obs_multi = await self.get_by_id_multi(
            db=db,
            ids_=ids_,
        )
        pulsars: dict[int, dict[str, Any]] = {}

        for obs in obs_multi:
            params = GetKnownPulsarQueryParams(
                ra=obs.s_ra,
                dec=obs.s_dec,
                radius=radius,
            )

            pulsars_in_obs_region = await self.pulsar_controller.get_all(
                db=db,
                q=[params],
            )

            if len(pulsars_in_obs_region) == 0:
                continue

            obs_dict = as_dict(obs)

            for pulsar in pulsars_in_obs_region:
                pulsar_id = pulsar.id

                if pulsar_id not in pulsars:
                    pulsars[pulsar_id] = {
                        "source": as_dict(pulsar),
                        "observations": [],
                    }

                pulsars[pulsar_id]["observations"].append(obs_dict)

        return list(pulsars.values())
