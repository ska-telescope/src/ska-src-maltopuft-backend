"""Extras used in the label service."""

import string
from typing import Annotated

from pydantic import AfterValidator, StringConstraints


def is_css_str(v: str) -> str:
    """Assert a hex color string can be parsed from a string input."""
    v = v.lstrip("#")
    css_str_len = 6
    if len(v) != css_str_len or not all(c in string.hexdigits for c in v):
        msg = "Invalid CSS color string"
        raise ValueError(msg)
    return v


CssColorStr = Annotated[
    str,
    StringConstraints(
        strip_whitespace=True,
        min_length=6,
        max_length=7,
        pattern=r"^(#)?([A-Fa-f0-9]{6})$",
    ),
    AfterValidator(is_css_str),
]
