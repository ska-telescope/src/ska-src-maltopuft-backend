"""Label service response schemas."""

from pydantic import BaseModel, PastDatetime, PositiveInt

from ska_src_maltopuft_backend.candle.responses import CandidateNested
from ska_src_maltopuft_backend.core.custom_types import PositiveList
from ska_src_maltopuft_backend.user.responses import User

from .css_color import CssColorStr
from .entity import EntityNames


class Entity(BaseModel):
    """Response model for Entity HTTP GET/POST requests."""

    id: PositiveInt
    type: EntityNames
    css_color: CssColorStr
    created_at: PastDatetime
    updated_at: PastDatetime


class Label(BaseModel):
    """Response model for Label HTTP GET/POST requests."""

    id: PositiveInt
    labeller_id: PositiveInt
    candidate_id: PositiveInt
    entity_id: PositiveInt
    created_at: PastDatetime
    updated_at: PastDatetime

    candidate: CandidateNested
    entity: Entity
    labeller: User


class LabelBulk(BaseModel):
    """Response model for bulk create Label requests."""

    ids: PositiveList[int]
