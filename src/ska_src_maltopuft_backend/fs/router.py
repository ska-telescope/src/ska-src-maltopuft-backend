"""Routers for remote filesystem endpoints."""

from typing import Annotated

from fastapi import APIRouter, Depends

from ska_src_maltopuft_backend.core.factory import Factory

from .controller import FileSystemController
from .requests import GetSubplotQueryParameters

fs_router = APIRouter()


@fs_router.get("/download")
async def download(
    subplot_data: Annotated[GetSubplotQueryParameters, Depends()],
    controller: Annotated[
        FileSystemController,
        Depends(
            Factory().get_filesystem_controller,
        ),
    ],
) -> dict[str, str]:
    """Stream the bytes from DIDs in a Rucio Storage Element."""
    return await controller.get_multi_files_from(
        urls=subplot_data.plot_path,
    )
