"""Filesystem HTTP request models."""

from typing import Annotated

from fastapi import Query
from pydantic import BaseModel, Field, StringConstraints


class GetSubplotQueryParameters(BaseModel):
    """Query parameters for the GET /download endpoint."""

    plot_path: list[
        (Annotated[str, StringConstraints(strip_whitespace=True)])
    ] = Field(Query(default=[]))
