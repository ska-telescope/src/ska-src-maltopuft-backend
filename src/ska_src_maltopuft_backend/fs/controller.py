"""Methods for interacting with a local or remote filesystem
over HTTP.
"""

import asyncio
import base64
from io import BytesIO

from fastapi import HTTPException, status
from httpx import AsyncClient, HTTPStatusError

from ska_src_maltopuft_backend.core.exceptions import (
    AuthenticationRequiredError,
    NotFoundError,
)
from ska_src_maltopuft_backend.core.token_manager.token_manager import (
    TokenManager,
    get_token_manager,
)


class FileSystemController:
    """Methods for interacting with a local or remote filesystem
    over HTTP.
    """

    def __init__(
        self,
        token_manager: TokenManager = get_token_manager(),
    ) -> None:
        """Initialise a FileSystemController instance."""
        self.chunksize: int = 4096
        self._token_manager: TokenManager = token_manager

    async def get_file_from(
        self,
        client: AsyncClient,
        url: str,
    ) -> str:
        """Stream file data over HTTPS and return as base64 encoded
        string.
        """
        try:
            result = await client.get(url=url, timeout=10)
            result.raise_for_status()
        except HTTPStatusError as exc:
            if exc.response.status_code == status.HTTP_401_UNAUTHORIZED:
                raise AuthenticationRequiredError from exc
            if exc.response.status_code in (
                status.HTTP_403_FORBIDDEN,
                status.HTTP_404_NOT_FOUND,
            ):
                raise NotFoundError from exc
            raise HTTPException(
                status_code=status.HTTP_502_BAD_GATEWAY,
                detail=(
                    "Error retrieving file from the remote server. If this "
                    f"problem persists, please contact support: {exc!s}",
                ),
            ) from exc

        buff = BytesIO()
        async for chunk in result.aiter_bytes(self.chunksize):
            buff.write(chunk)

        return base64.b64encode(buff.getvalue()).decode("utf-8")

    async def get_multi_files_from(
        self,
        urls: list[str],
    ) -> dict[str, str]:
        """Stream data for several files over HTTP and return as a dict of
        base64 encoded strings.
        """
        if len(urls) == 0:
            return {}

        headers = await self._token_manager.prepare_auth_header()
        async with AsyncClient(
            headers=headers,
            verify=False,  # noqa: S501
        ) as client:
            tasks = [
                self.get_file_from(client=client, url=url) for url in urls
            ]
            results = await asyncio.gather(*tasks)
        return {
            url: result
            for url, result in zip(urls, results, strict=False)
            if result
        }
