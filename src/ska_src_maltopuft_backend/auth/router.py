"""Routers for OIDC login endpoints."""

import logging
from typing import Any
from urllib.parse import urlencode

from fastapi import APIRouter
from httpx import AsyncClient

from ska_src_maltopuft_backend.core.config import settings

from .responses import AuthUrl, TokenResponse

logger = logging.getLogger(__name__)
auth_router = APIRouter()


@auth_router.get("/login", response_model=AuthUrl)
async def login(redirect_uri: str) -> Any:
    """Re-direct user to login securely via SKA IAM."""
    query_params = {
        "response_type": "code",
        "client_id": settings.AUTH.MALTOPUFT_IAM_CLIENT_ID,
        "redirect_uri": redirect_uri,
        "scope": settings.AUTH.MALTOPUFT_IAM_CLIENT_SCOPES,
    }
    auth_url = (
        f"https://ska-iam.stfc.ac.uk/authorize?{urlencode(query_params)}"
    )
    return {"auth_url": auth_url}


@auth_router.get("/token", response_model=TokenResponse)
async def auhtorize_token(code: str, redirect_uri: str) -> dict:
    """Get token from SKA IAM."""
    data = {
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": redirect_uri,
        "client_id": settings.AUTH.MALTOPUFT_IAM_CLIENT_ID,
        "client_secret": settings.AUTH.MALTOPUFT_IAM_CLIENT_SECRET,
    }

    async with AsyncClient() as client:
        token = await client.post(
            url=f"{settings.AUTH.MALTOPUFT_IAM_SERVER}/token",
            data=data,
        )
        token.raise_for_status()

    return {"token": token.json()}
