"""Auth service responses schemas."""

from pydantic import BaseModel, PositiveInt


class AuthUrl(BaseModel):
    """Authentication URL."""

    auth_url: str


class Token(BaseModel):
    """SKA-IAM OIDC token."""

    access_token: str
    token_type: str
    expires_in: PositiveInt
    scope: str
    refresh_token: str | None = None


class TokenResponse(BaseModel):
    """SKA-IAM OIDC /token response model."""

    token: Token
