"""Pydantic models describing the schemas for mock data used in tests. Several
models that are defined in the main application are imported here but not
explictly used in this module to provide a common place from which test
data models can be imported. Any models defined in the core application that
can be reused to describe test data schemas should be imported here. Models
that do not need to be defined in the main application code should also be
defined in this module. These models are used subclassed by polyfactory test
data factories in the tests.mocks.factory module.
"""

import datetime as dt
from typing import Annotated

from annotated_types import Ge, Le
from pydantic import (
    BaseModel,
    HttpUrl,
    IPvAnyAddress,
    PastDatetime,
    PositiveFloat,
    PositiveInt,
)

from ska_src_maltopuft_backend.app.schemas.requests import (  # noqa: F401 (unused-import)
    CreateCandidate,
    CreateEntity,
    CreateLabel,
    CreateSPCandidate,
    CreateUser,
)
from ska_src_maltopuft_backend.core.custom_types import (
    DeclinationDegrees,
    RightAscensionDegrees,
)
from ska_src_maltopuft_backend.core.schemas import RaDecPositionBase


class CreateScheduleBlock(BaseModel):
    """Test ScheduleBlock data generator model."""

    start_at: PastDatetime
    est_end_at: dt.datetime


class CreateMeerkatScheduleBlock(BaseModel):
    """Test MeerkatScheduleBlock data generator model."""

    meerkat_id: PositiveInt
    meerkat_id_code: str
    proposal_id: str
    schedule_block_id: PositiveInt


class CreateCoherentBeamConfig(BaseModel):
    """Test CoherentBeamConfig data generator model."""

    angle: float
    fraction_overlap: Annotated[float, Ge(0), Le(1), lambda x: round(x, 5)]
    x: float
    y: float


class CreateObservation(BaseModel):
    """Test Observation data generator model."""

    dataproduct_type: str | None = None
    dataproduct_subtype: str | None = None
    calib_level: int | None = None
    obs_id: str | None = None
    s_ra: RightAscensionDegrees
    s_dec: DeclinationDegrees
    t_min: PastDatetime
    t_max: dt.datetime
    t_exptime: dt.timedelta | None = None
    t_resolution: float | None = None
    em_min: float | None = None
    em_max: float | None = None
    em_resolution: float | None = None
    em_xel: int | None = None
    pol_states: str | None = None
    pol_xel: int | None = None
    facility_name: str | None = None
    instrument_name: str | None = None
    target_name: str | None = None
    target_class: str | None = None

    coherent_beam_config_id: PositiveInt
    schedule_block_id: PositiveInt


class CreateTilingConfig(BaseModel):
    """Test TilingConfig data generator model."""

    coordinate_type: str
    epoch: float
    epoch_offset: float
    method: str
    nbeams: int
    overlap: float
    reference_frequency: float
    shape: str
    target: str
    ra: RightAscensionDegrees
    dec: DeclinationDegrees

    observation_id: PositiveInt


class CreateBeam(BaseModel):
    """Test Beam data generator model."""

    number: PositiveInt
    coherent: bool
    ra: RightAscensionDegrees
    dec: DeclinationDegrees

    host_id: PositiveInt
    observation_id: PositiveInt


class CreateHost(BaseModel):
    """Test Host data generator model."""

    ip_address: IPvAnyAddress
    hostname: str
    port: PositiveInt


class CreateKnownPulsar(RaDecPositionBase):
    """Test KnownPulsar data generator model."""

    name: str
    dm: PositiveFloat
    width: PositiveFloat
    period: PositiveFloat
    catalogue_id: PositiveInt


class CreateCatalogue(BaseModel):
    """Test Catalogue data generator model."""

    name: str
    url: HttpUrl
