"""Test data factories for Pydantic models."""

# ruff: noqa: D101, D102

import json
from typing import Any

from polyfactory.factories.pydantic_factory import ModelFactory

from . import models as test_models


class UserFactory(ModelFactory[test_models.CreateUser]):
    """CreateUser Pydantic model factory."""


class LabelFactory(ModelFactory[test_models.CreateLabel]):
    """CreateLabel Pydantic model factory."""


class EntityFactory(ModelFactory[test_models.CreateEntity]):
    """CreateEntity Pydantic model factory."""


class ScheduleBlockFactory(ModelFactory[test_models.CreateScheduleBlock]):
    """CreateScheduleBlock Pydantic model factory."""


class ObservationFactory(ModelFactory[test_models.CreateObservation]):
    """Observation Pydantic model factory."""


class CoherentBeamConfigFactory(
    ModelFactory[test_models.CreateCoherentBeamConfig],
):
    """CoherentBeamConfig Pydantic model factory."""


class HostFactory(ModelFactory[test_models.CreateHost]):
    """Host Pydantic model factory."""


class BeamFactory(ModelFactory[test_models.CreateBeam]):
    """Beam Pydantic model factory."""


class CandidateFactory(ModelFactory[test_models.CreateCandidate]):
    """CreateCandidate Pydantic model factory."""


class SPCandidateFactory(ModelFactory[test_models.CreateSPCandidate]):
    """CreateSPCandidate Pydantic model factory."""


class KnownPulsarFactory(ModelFactory[test_models.CreateKnownPulsar]):
    """CreateKnownPulsar Pydantic model factory."""


class CatalogueFactory(ModelFactory[test_models.CreateCatalogue]):
    """CreateCatalogue Pydantic model factory."""


class TestDataFactory:
    """Factory for generating test data."""

    def _build_data(
        self,
        factory: type[ModelFactory[Any]],
        disable_validation: bool = False,  # noqa: FBT001, FBT002
        **kwargs: Any,
    ) -> dict[str, Any]:
        data = factory.build(
            factory_use_construct=disable_validation,
            **kwargs,
        )
        return json.loads(data.model_dump_json())

    def user(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(UserFactory, **kwargs)

    def label(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(LabelFactory, **kwargs)

    def entity(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(EntityFactory, **kwargs)

    def schedule_block(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(ScheduleBlockFactory, **kwargs)

    def observation(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(ObservationFactory, **kwargs)

    def coherent_beam_config(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(CoherentBeamConfigFactory, **kwargs)

    def host(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(HostFactory, **kwargs)

    def beam(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(BeamFactory, **kwargs)

    def candidate(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(CandidateFactory, **kwargs)

    def sp_candidate(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(SPCandidateFactory, **kwargs)

    def pulsar(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(KnownPulsarFactory, **kwargs)

    def catalogue(self, **kwargs: Any) -> dict[str, Any]:
        return self._build_data(CatalogueFactory, **kwargs)


test_data_factory = TestDataFactory()
