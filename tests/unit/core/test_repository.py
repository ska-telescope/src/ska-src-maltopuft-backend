"""Base CRUD repository unit tests."""

#  ruff: noqa: SLF001, PLR2004

from typing import Any

import pytest
import pytest_asyncio
import sqlalchemy as sa
from sqlalchemy import Select

from ska_src_maltopuft_backend.app.models import User
from ska_src_maltopuft_backend.core.repository import BaseRepository


@pytest_asyncio.fixture(scope="module")
async def repository() -> BaseRepository:
    """User repository fixture."""
    return BaseRepository(model=User)


def test_apply_pagination_with_default_values(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When skip and limit are not present in the query parameters,
    Then the `LIMIT` and `OFFSET` should be set to the default values.
    """
    query: Select = Select(User)
    query = repository._apply_pagination(query=query, q={})
    assert query._limit == 100
    assert query._offset == 0


def test_apply_pagination_with_non_default_values(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When skip and limit are present in the query parameters,
    Then the `LIMIT` and `OFFSET` should be set.
    """
    query: Select = Select(User)

    skip = 3
    limit = 4
    query = repository._apply_pagination(
        query=query,
        q={
            "skip": skip,
            "limit": limit,
        },
    )

    assert query._offset is skip
    assert query._limit is limit


def test_add_joins_to_query(repository: BaseRepository) -> None:
    """Given a query,
    When an attempt is made to add valid joins to the query,
    Then the joins should be present in the query.
    """
    query: Select = Select(User)
    join_ = ["label", "entity"]

    query = repository._apply_joins(query=query, join_=join_)
    query_joins = [j[0].description for j in query._setup_joins]  # type: ignore[attr-defined]
    for j in join_:
        assert j in query_joins


def test_add_empty_join_to_query(repository: BaseRepository) -> None:
    """Given a query,
    When empty joins are made on the query,
    Then the original query should not be changed.
    """
    query: Select = Select(User)
    query_after_join = repository._apply_joins(query=query, join_=[])
    assert query == query_after_join


def test_add_invalid_table_to_join(repository: BaseRepository) -> None:
    """Given a query,
    When list of joins contains an invalid table,
    Then a ValueError should be raised.
    """
    query: Select = Select(User)
    invalid_table_name = "this_table_doesnt_exist"
    join_ = [invalid_table_name]
    with pytest.raises(
        ValueError,
        match=f"Invalid table name '{invalid_table_name}' provided",
    ):
        repository._apply_joins(query=query, join_=join_)  # type: ignore[arg-type]


def test_add_invalid_join_type_to_query(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When joins are specified in a set rather than a list,
    Then a TypeError should be raised.
    """
    query: Select = Select(User)
    join_ = {"label"}
    join_type = type(join_)
    with pytest.raises(
        TypeError,
        match=f"join_ parameter should be a list, found {join_type}",
    ):
        repository._apply_joins(query=query, join_=join_)  # type: ignore[arg-type]


def test_add_duplicate_join_to_query(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When the joins contain a duplicated table,
    Then a TypeError should be raised.
    """
    query: Select = Select(User)
    join_ = ["label", "label"]
    with pytest.raises(
        ValueError,
        match="Duplicate table found in list of joins",
    ):
        repository._apply_joins(query=query, join_=join_)  # type: ignore[arg-type]


def test_single_apply_filters(repository: BaseRepository) -> None:
    """Given a query,
    When a single value of ID is provided as a predicate,
    Then a user.id should be added to the where clause.
    """
    query: Select = Select(User)
    filters = {"id": 1}
    filtered_query = repository._apply_filters(query=query, q=filters)
    expected_query = query.where(User.id == filters["id"])
    assert str(filtered_query) == str(expected_query)


def test_apply_filters_with_no_predicates(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When no predicates are given to the query,
    Then the original query should not be changed.
    """
    query: Select = Select(User)
    query_after_predicate = repository._apply_filters(query=query, q={})
    assert query == query_after_predicate


def test_apply_filters_with_skip_and_limit(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When skip and limits are present in the query parameters,
    Then applying query filters should not change the original query.
    """
    query: Select = Select(User)
    query_after_predicate = repository._apply_filters(
        query=query,
        q={
            "skip": 123,
            "limit": 123,
        },
    )

    assert query == query_after_predicate


def test_apply_filters_with_null_predicate(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When a predicate with value `None` is provided,
    Then the original query should not be changed.
    """
    query: Select = Select(User)
    query_after_predicate = repository._apply_filters(
        query=query,
        q={"predicate": None},
    )
    assert query == query_after_predicate


def test_apply_filters_with_empty_param_list(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When an empty list is provided as a query parameter,
    Then the original query should not be changed.
    """
    query: Select = Select(User)
    query_after = repository._apply_filters(query=query, q={"id": []})
    assert query == query_after


def test_apply_filters_with_id_param_list(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When a list of ids is provided as a query parameter,
    Then an user.id IN clause should be added to the query.
    """
    query: Select = Select(User)
    filters = {"id": [1, 2, 3]}
    filtered_query = repository._apply_filters(query=query, q=filters)
    expected_query = query.where(User.id.in_(filters["id"]))
    assert str(filtered_query) == str(expected_query)


def test_apply_filters_with_str_param_list(
    repository: BaseRepository,
) -> None:
    """Given a query,
    When a list of username strings is provided as a query parameter,
    Then an user.username IN clause should be added to the query.
    """
    query: Select = Select(User)
    filters = {"username": ["a", "b", "c"]}
    filtered_query = repository._apply_filters(
        query=query,
        q=filters,
    )
    expected_query = query.where(User.username.in_(filters["username"]))
    assert str(filtered_query) == str(expected_query)


def test_apply_foreign_key_filter(repository: BaseRepository) -> None:
    """Given a query,
    When a list of user_id foreign keys is provided as a query parameter,
    Then an user.user_id IN clause should be added to the query.
    """
    query: Select = Select(User)
    filters = {"user_id": [1, 2, 3]}
    filtered_query = repository._apply_filters(
        query=query,
        q=filters,
    )
    expected_query = query.where(User.id.in_([1, 2, 3]))
    assert str(filtered_query) == str(expected_query)


def test_ignore_invalid_filters(repository: BaseRepository) -> None:
    """Given a query,
    When invalid (None or empty list) query parameters are given,
    The the query should not be changed.
    """
    query: Select = Select(User)
    filters: dict[str, Any] = {"username": None, "is_admin": []}
    filtered_query = repository._apply_filters(query=query, q=filters)
    assert str(filtered_query) == str(query)


def test_has_pos_filter_with_no_params(repository: BaseRepository) -> None:
    """No pos filter params present returns False."""
    q: dict[str, Any] = {}
    assert not repository._has_pos_filter_params(q=q)


def test_has_pos_filter_with_invalid_params(
    repository: BaseRepository,
) -> None:
    """Present pos filter params returns True, even if they are invalid."""
    q: dict[str, Any] = {
        "ra": None,
        "dec": [],
        "pos": "(None,[])",
        "radius": None,
    }
    assert repository._has_pos_filter_params(q=q)


def test_has_pos_filter_with_missing_param(repository: BaseRepository) -> None:
    """Missing pos filter param returns False."""
    q: dict[str, Any] = {
        "ra": 90.75270833,
        "dec": -40.05644444,
        "pos": "(6h03m00.65s,-40d03m23.2s)",
    }
    assert not repository._has_pos_filter_params(q=q)


def test_has_pos_filter_with_valid_params(repository: BaseRepository) -> None:
    """Present and valid pos filter params returns True."""
    q: dict[str, Any] = {
        "ra": 90.75270833,
        "dec": -40.05644444,
        "pos": "(6h03m00.65s,-40d03m23.2s)",
        "radius": 1,
    }
    assert repository._has_pos_filter_params(q=q)


def test_positive_partial_match_condition(repository: BaseRepository) -> None:
    """Given a query key ending in _like
    And a list of string values
    When the partial match condition is checked
    Then it should return True.
    """
    key = "username_like"
    value = ["name"]
    assert repository._is_partial_match_filter(key=key, values=value)


def test_negative_partial_match_condition(repository: BaseRepository) -> None:
    """Given a query key that doesn't end in _like
    And a list of string values
    When the partial match condition is checked
    Then it should return False.
    """
    key = "username"
    value = ["name"]
    assert not repository._is_partial_match_filter(key=key, values=value)


def test_partial_match_query(repository: BaseRepository) -> None:
    """Given a query key ending in _like
    And a list containing a string value with a partial match
    When the partial match query is created
    Then it should return a query with a LIKE condition.
    """
    expected_query: Select = sa.Select(User).where(User.username.like("%name"))

    query: Select = Select(User)
    query = repository._apply_filters(
        query=query,
        q={"username_like": ["%name"]},
    )
    assert "username LIKE :username_1" in str(query)
    assert str(query) == str(expected_query)


def test_partial_match_multi_query(repository: BaseRepository) -> None:
    """Given a query key ending in _like
    And a list of several string values with a partial match
    When the partial match query is created
    Then it should return a query with several LIKE conditions.
    """
    expected_query: Select = sa.Select(User).where(
        User.username.like("%name"),
        User.username.like("%name2"),
    )

    query: Select = Select(User)
    query = repository._apply_filters(
        query=query,
        q={"username_like": ["%name", "%name2"]},
    )
    assert "username LIKE :username_1" in str(query)
    assert "username LIKE :username_2" in str(query)
    assert str(query) == str(expected_query)


def test_match_and_partial_match_on_same_attribute(
    repository: BaseRepository,
) -> None:
    """Given a query key ending in _like
    And the equivalent key without _like
    When query is created
    Then it should return a query with a LIKE condition
    And it should return a query with an equality condition.
    """
    expected_query: Select = sa.Select(User).where(
        User.username == "name",
        User.username.like("%name2"),
    )

    query: Select = Select(User)
    query = repository._apply_filters(
        query=query,
        q={
            "username": "name",
            "username_like": ["%name2"],
        },
    )
    assert "username = :username_1" in str(query)
    assert "username LIKE :username_2" in str(query)
    assert str(query) == str(expected_query)
