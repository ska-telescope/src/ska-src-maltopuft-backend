"""Unit tests for the TokenManager class."""

# ruff: noqa: D103

import time
from json import JSONDecodeError
from unittest import mock

import pytest
from httpx import AsyncClient, HTTPStatusError

from ska_src_maltopuft_backend.core.config import settings
from ska_src_maltopuft_backend.core.exceptions import (
    InvalidClientTokenError,
    TokenProviderResponseError,
)
from ska_src_maltopuft_backend.core.token_manager import TokenManager


@pytest.mark.asyncio
async def test_refresh_client_token_is_success(
    mock_token_manager: TokenManager,
) -> None:
    # Mock a successful response from the token provider
    mock_response = mock.Mock()
    mock_response.json.return_value = {
        "access_token": "mock-access-token",
        "expires_in": 3600,
        "token_type": "client_credentials",
        "scope": "my_scope",
    }

    with mock.patch.object(AsyncClient, "post", return_value=mock_response):
        await mock_token_manager._refresh_token()  # noqa: SLF001

    # Check the token and expiry time have been set correctly
    assert mock_token_manager.token == "mock-access-token"  # noqa: S105
    assert mock_token_manager.token_expires_at > int(time.time())


@pytest.mark.asyncio
async def test_refresh_client_token_http_error(
    mock_token_manager: TokenManager,
) -> None:
    # Mock an error response from the token provider
    mock_response = mock.Mock()
    mock_response.raise_for_status.side_effect = HTTPStatusError(
        message="Error",
        request=mock.Mock(),
        response=mock.Mock(status_code=400, text="Bad Request"),
    )

    with (
        mock.patch.object(AsyncClient, "post", return_value=mock_response),
        pytest.raises(TokenProviderResponseError),
    ):
        await mock_token_manager._refresh_token()  # noqa: SLF001

    # Check that the token and expiry time have been reset
    assert mock_token_manager.token is None
    assert mock_token_manager.token_expires_at == 0


@pytest.mark.asyncio
async def test_refresh_client_token_json_decode_error(
    mock_token_manager: TokenManager,
) -> None:
    # Mock a response which passes raise_for_status() and raises a JSONDecodeError
    mock_response = mock.Mock()
    mock_response.raise_for_status = mock.Mock()
    mock_response.json.side_effect = JSONDecodeError(
        msg="Error",
        doc="some-json-response",
        pos=0,
    )

    with (
        mock.patch.object(AsyncClient, "post", return_value=mock_response),
        pytest.raises(InvalidClientTokenError),
    ):
        await mock_token_manager._refresh_token()  # noqa: SLF001

    # Check that the token and expiry time have been reset
    assert mock_token_manager.token is None
    assert mock_token_manager.token_expires_at == 0


@pytest.mark.asyncio
async def test_refresh_client_token_validation_error(
    mock_token_manager: TokenManager,
) -> None:
    # Mock a response which passes raise_for_status() and returns an invalid token
    # (i.e. raises a validation error when trying to parse the response with the
    # ClientAccessToken Pydantic model)
    mock_response = mock.Mock()
    mock_response.raise_for_status = mock.Mock()
    mock_response.json.return_value = {
        "invalid_key": "invalid_value",
    }

    with (
        mock.patch.object(AsyncClient, "post", return_value=mock_response),
        pytest.raises(InvalidClientTokenError),
    ):
        await mock_token_manager._refresh_token()  # noqa: SLF001

    # Check that the token and expiry time have been reset
    assert mock_token_manager.token is None
    assert mock_token_manager.token_expires_at == 0


@pytest.mark.asyncio
async def test_prepare_auth_header_returns_bearer_token_when_auth_enabled(
    mock_token_manager: TokenManager,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    # Enable authentication
    monkeypatch.setattr(
        settings,
        "AUTH_DISABLED",
        0,
    )

    # Mock a successful response from the token provider
    mock_response = mock.Mock()
    mock_response.json.return_value = {
        "access_token": "mock-access-token",
        "expires_in": 3600,
        "token_type": "client_credentials",
        "scope": "my_scope",
    }

    with mock.patch.object(AsyncClient, "post", return_value=mock_response):
        assert await mock_token_manager.prepare_auth_header() == {
            "Authorization": "Bearer mock-access-token",
        }


@pytest.mark.asyncio
async def test_prepare_auth_header_returns_empty_dict_if_auth_disabled(
    mock_token_manager: TokenManager,
) -> None:
    assert await mock_token_manager.prepare_auth_header() == {}
