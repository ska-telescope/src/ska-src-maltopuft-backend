# ruff: noqa: ARG001, ANN001, ANN202, D100, D103, S106, SLF001
from unittest import mock

import pytest
from fastapi import HTTPException
from httpx import AsyncClient, HTTPStatusError

from ska_src_maltopuft_backend.core.exceptions import (
    AuthenticationRequiredError,
    NotFoundError,
)
from ska_src_maltopuft_backend.core.token_manager.token_manager import (
    TokenManager,
)
from ska_src_maltopuft_backend.fs.controller import FileSystemController


@pytest.fixture
def controller(mock_token_manager) -> FileSystemController:
    """Instantiate a FileSystemController test fixture with a mock TokenManager."""
    return FileSystemController(token_manager=mock_token_manager)


def test_filesystem_controller_init(controller: FileSystemController) -> None:
    """Test that the TokenManager is initialised correctly."""
    assert isinstance(controller._token_manager, TokenManager)


@pytest.mark.asyncio
async def test_get_file_from_success(controller: FileSystemController) -> None:
    """Test that get_file_from returns the base64 encoded file content."""

    async def mock_aiter_bytes(self):
        yield b"file-content"

    mock_response = mock.Mock()
    mock_response.aiter_bytes = mock_aiter_bytes
    mock_response.raise_for_status = mock.Mock()

    with mock.patch.object(AsyncClient, "get", return_value=mock_response):
        result = await controller.get_file_from(
            client=AsyncClient(),
            url="https://example.com",
        )
        assert result == "ZmlsZS1jb250ZW50"


@pytest.mark.asyncio
async def test_get_file_from_unauthorized(
    controller: FileSystemController,
) -> None:
    """Test that an HTTP 401 response from the remote filesystem raises an
    AuthenticationRequiredError.
    """
    mock_response = mock.Mock()
    mock_response.raise_for_status = mock.Mock(
        side_effect=HTTPStatusError(
            "Unauthorized",
            request=None,  # type: ignore[arg-type]
            response=mock.Mock(status_code=401),
        ),
    )

    with (
        mock.patch.object(AsyncClient, "get", return_value=mock_response),
        pytest.raises(AuthenticationRequiredError),
    ):
        await controller.get_file_from(
            client=AsyncClient(),
            url="https://example.com/file",
        )


@pytest.mark.asyncio
async def test_get_file_from_not_found(
    controller: FileSystemController,
) -> None:
    """Test that an HTTP 403 or 404 response from the remote filesystem raises
    a NotFoundError.
    """
    mock_response = mock.Mock()
    mock_response.raise_for_status = mock.Mock(
        side_effect=HTTPStatusError(
            "Error",
            request=None,  # type: ignore[arg-type]
            response=mock.Mock(status_code=404),
        ),
    )

    with (
        mock.patch.object(AsyncClient, "get", return_value=mock_response),
        pytest.raises(NotFoundError),
    ):
        await controller.get_file_from(
            client=AsyncClient(),
            url="https://example.com/file",
        )


@pytest.mark.asyncio
async def test_get_file_from_other_error(
    controller: FileSystemController,
) -> None:
    """Test that any HTTP error that is not 401, 403, or 404 raises an HTTPException
    with status code 502 and the given error message.
    """
    mock_response = mock.Mock()
    mock_response.raise_for_status = mock.Mock(
        side_effect=HTTPStatusError(
            "Bad Gateway",
            request=None,  # type: ignore[arg-type]
            response=mock.Mock(status_code=502),
        ),
    )

    err_msg = (
        r"Error retrieving file from the remote server. "
        r"If this problem persists, please contact support:.*?"
    )
    with (
        mock.patch.object(AsyncClient, "get", return_value=mock_response),
        pytest.raises(HTTPException, match=err_msg),
    ):
        await controller.get_file_from(
            client=AsyncClient(),
            url="https://example.com/file",
        )


@pytest.mark.asyncio
async def test_no_urls(controller: FileSystemController) -> None:
    """Test that an empty list of URLs returns an empty dictionary."""
    response = await controller.get_multi_files_from(urls=[])
    assert response == {}


@pytest.mark.asyncio
async def test_get_files_from_success(
    controller: FileSystemController,
) -> None:
    """Test that get_multi_files_from returns a dictionary of URLs and their
    base64 encoded contents.
    """

    async def mock_aiter_bytes(self):
        yield b"file-content"

    mock_response = mock.Mock()
    mock_response.aiter_bytes = mock_aiter_bytes
    mock_response.raise_for_status = mock.Mock()

    with mock.patch.object(AsyncClient, "get", return_value=mock_response):
        result = await controller.get_multi_files_from(
            urls=["https://example1.com", "https://example2.com"],
        )
        assert result == {
            "https://example1.com": "ZmlsZS1jb250ZW50",
            "https://example2.com": "ZmlsZS1jb250ZW50",
        }
