"""Candidate service API tests."""

# ruff: noqa: D103

import ast
from typing import Any

from fastapi.testclient import TestClient
from pytest_bdd import given, parsers, scenarios, then, when

from ska_src_maltopuft_backend.app.schemas.responses import Candidate
from tests.mocks.factory import test_data_factory

scenarios("./candidate_api.feature")


@given(parsers.parse("a candidate where {attributes} is {values}"))
def candidate_with_attributes(
    result: dict[str, Any],
    attributes: str,
    values: Any,
) -> None:
    """Create a candidate model with the given attributes."""
    cand_attributes = dict(
        zip(
            ast.literal_eval(attributes),
            ast.literal_eval(values),
            strict=False,
        ),
    )
    result["candidate"] = test_data_factory.candidate(
        beam_id=result["beam"]["id"],  # type: ignore[arg-type]
        **cand_attributes,
    )


@when("candidates are retrieved from the database")
def do_get_candidates(
    client: TestClient,
    result: dict[str, Any],
) -> None:
    result["result"] = client.get(url="/v1/candle", params=result.get("q"))


@when("an attempt is made to create the candidate")
def do_create_candidate(
    client: TestClient,
    result: dict[str, Any],
) -> None:
    result["result"] = client.post(
        url="/v1/candle",
        json=result["candidate"],
    )


@when("the candidate is retrieved from the database by id")
def do_get_candidate_by_id(
    client: TestClient,
    result: dict[str, Any],
) -> None:
    result["result"] = client.get(url="/v1/candle/1")


@when("an attempt is made to delete the candidate from the database")
def do_delete_candidate(
    client: TestClient,
    result: dict[str, Any],
) -> None:
    result["result"] = client.delete(url="/v1/candle/1")


@then(parsers.parse("the response data should contain {num:d} candidates"))
def response_data_has_num_cand(result: dict[str, Any], num: int) -> None:
    response = result["response"]
    data = response.json()

    if isinstance(data, dict):
        data = [data]

    assert len(data) == int(num)
    for d in data:
        cand = Candidate(**d)
        assert cand.id is not None
