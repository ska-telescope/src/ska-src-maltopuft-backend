"""BDD test steps shared between candle features."""

from typing import Any

from fastapi.testclient import TestClient
from pytest_bdd import given

from tests.mocks.factory import test_data_factory


@given("a candidate")
def cand_data(result: dict[str, Any]) -> None:
    """Generate fake candidate data."""
    result["candidate"] = test_data_factory.candidate(
        beam_id=result["beam"]["id"],  # type: ignore[arg-type]
    )


@given("the candidate exists in the database")
def candidate_exists(result: dict[str, Any], client: TestClient) -> None:
    """Take candidate from the 'result' fixture and create it."""
    client.post(url="/v1/candle", json=result["candidate"])


@given("a sp candidate")
def sp_candidate_and_parent_data(
    result: dict[str, Any],
    client: TestClient,
) -> None:
    """Generate fake candidate and sp candidates."""
    result["candidate"] = test_data_factory.candidate(
        beam_id=result["beam"]["id"],
    )
    cand = client.post(url="/v1/candle", json=result["candidate"]).json()
    result["sp_candidate"] = test_data_factory.sp_candidate(
        candidate_id=cand["id"],
    )


@given("the sp candidate exists in the database")
def sp_candidate_exists(result: dict[str, Any], client: TestClient) -> None:
    """Take sp candidate from the 'result' fixture and create it."""
    client.post(url="/v1/candle/sp", json=result["sp_candidate"])
