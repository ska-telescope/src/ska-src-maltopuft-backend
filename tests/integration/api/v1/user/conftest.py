"""BDD test steps shared between user features."""

from typing import Any

from pytest_bdd import given

from tests.mocks.factory import test_data_factory


@given("a user")
def user_data(result: dict[str, Any]) -> None:
    """Generate fake user data."""
    result["user"] = test_data_factory.user()
