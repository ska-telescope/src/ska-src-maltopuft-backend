"""BDD test steps shared between label features."""

from typing import Any

from fastapi.testclient import TestClient
from pytest_bdd import given

from tests.mocks.factory import test_data_factory


@given("an 'RFI' entity")
def rfi_data(result: dict[str, Any]) -> None:
    """Generate fake RFI entity data."""
    result["entity"] = test_data_factory.entity(type="RFI")


@given("a 'SINGLE_PULSE' entity")
def sp_data(result: dict[str, Any]) -> None:
    """Generate fake SINGLE_PULSE entity data."""
    result["entity"] = test_data_factory.entity(type="SINGLE_PULSE")


@given("a 'PERIODIC_PULSE' entity")
def periodic_data(result: dict[str, Any]) -> None:
    """Generate fake PERIODIC_PULSE entity data."""
    result["entity"] = test_data_factory.entity(type="PERIODIC_PULSE")


@given("the entity exists in the database")
def entity_exists(result: dict[str, Any], client: TestClient) -> None:
    """Take entity from the 'result' fixture and create it."""
    client.post(url="/v1/labels/entity", json=result.get("entity"))


@given("a label")
def label_and_parent_data(
    result: dict[str, Any],
    client: TestClient,
) -> None:
    """Generate fake label."""
    result["user"] = test_data_factory.user()
    result["candidate"] = test_data_factory.candidate(
        beam_id=result["beam"]["id"],
    )

    client.post(url="/v1/users", json=result["user"]).json()
    cand = client.post(url="/v1/candle", json=result["candidate"]).json()

    # Try to get an entity. If none exists, create one.
    entity = client.get(url="/v1/labels/entity/1").json()
    if entity.get("id") is None:
        result["entity"] = test_data_factory.entity()
        entity = client.post(
            url="/v1/labels/entity",
            json=result["entity"],
        ).json()

    result["label"] = test_data_factory.label(
        candidate_id=cand["id"],
        entity_id=entity["id"],
    )


@given("the label exists in the database")
def label_exists(result: dict[str, Any], client: TestClient) -> None:
    """Take label from the 'result' fixture and create it."""
    client.post(url="/v1/labels", json=result["label"])
