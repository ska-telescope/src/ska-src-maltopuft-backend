"""BDD test steps shared between observation features."""

import ast
from typing import Any

from fastapi.testclient import TestClient
from pytest_bdd import given, parsers, when
from sqlalchemy.orm import Session

from ska_src_maltopuft_backend.app.models import (
    Observation,
)
from tests.mocks.factory import test_data_factory


@given("an observation")
def obs_data(
    result: dict[str, Any],
) -> None:
    """Generate fake observation data."""
    result["obs"] = test_data_factory.observation(
        schedule_block_id=result["schedule_block"]["id"],
        coherent_beam_config_id=result["coherent_beam_config"]["id"],
    )


@given(parsers.parse("an observation where {attributes} is {values}"))
def obs_data_with_attr(
    result: dict[str, Any],
    attributes: str,
    values: Any,
) -> None:
    """Generate fake observation data with given attributes."""
    args = dict(
        zip(
            ast.literal_eval(attributes),
            ast.literal_eval(values),
            strict=False,
        ),
    )

    result["obs"] = test_data_factory.observation(
        schedule_block_id=result["schedule_block"]["id"],
        coherent_beam_config_id=result["coherent_beam_config"]["id"],
        **args,
    )


@given("the observation exists in the database")
def observation_metadata(db: Session, result: dict[str, Any]) -> None:
    """Create observation metadata in the database."""
    obs = Observation(**result["obs"])
    db.add(obs)
    db.commit()
    db.refresh(obs)
    result["obs"]["id"] = obs.id


@when("observations are retrieved from the database")
def do_get_obs(
    client: TestClient,
    result: dict[str, Any],
) -> None:
    """Retrieve observations from the database."""
    result["result"] = client.get(url="/v1/obs", params=result.get("q", []))
