"""Catalogue service API tests."""

# ruff: noqa: D103

import ast
from typing import Any

from fastapi.testclient import TestClient
from httpx import Response
from pytest_bdd import given, parsers, scenarios, then, when
from sqlalchemy.orm import Session

from ska_src_maltopuft_backend.app.models import (
    Catalogue as CatalogueSA,
)
from ska_src_maltopuft_backend.app.models import (
    KnownPulsar as KnownPulsarSA,
)
from ska_src_maltopuft_backend.app.schemas.responses import KnownPulsar
from tests.mocks.factory import test_data_factory

scenarios("./pulsar_catalogue_api.feature")


@given("a catalogue")
def catalogue(db: Session, result: dict[str, Any]) -> None:
    """Generate fake Catalogue data."""
    cat = test_data_factory.catalogue()
    cat_db = CatalogueSA(**cat)
    db.add(cat_db)
    db.commit()
    db.refresh(cat_db)
    cat["id"] = cat_db.id
    result["catalogue"] = cat


@given("a pulsar")
def pulsar_data(result: dict[str, Any]) -> None:
    """Generate fake KnownPulsar data."""
    cat = result["catalogue"]
    assert isinstance(cat, dict)
    result["pulsar"] = test_data_factory.pulsar(
        catalogue_id=cat["id"],
    )


@given(parsers.parse("a pulsar where {attributes} is {values}"))
def pulsar_with_attributes(
    result: dict[str, Any],
    attributes: str,
    values: Any,
) -> None:
    """Create a KnownPulsar model with the given attributes."""
    pulsar_attributes = dict(
        zip(
            ast.literal_eval(attributes),
            ast.literal_eval(values),
            strict=False,
        ),
    )

    cat = result["catalogue"]
    assert isinstance(cat, dict)
    result["pulsar"] = test_data_factory.pulsar(
        catalogue_id=cat["id"],
        **pulsar_attributes,
    )


@given("the pulsar exists in the database")
def pulsar(db: Session, result: dict[str, Any]) -> None:
    """Create pulsar in the database."""
    pulsar = result["pulsar"]
    pulsar_db = KnownPulsarSA(**pulsar)
    db.add(pulsar_db)
    db.commit()
    db.refresh(pulsar_db)
    result["pulsar"]["id"] = pulsar_db.id


@when("pulsars are retrieved from the database")
def do_get_pulsars(
    client: TestClient,
    result: dict[str, Any],
) -> None:
    result["result"] = client.get(
        url="/v1/catalogues/pulsars",
        params=result.get("q"),
    )


@then(parsers.parse("the response data should contain {num:d} pulsars"))
def response_data_has_num_pulsars(result: dict[str, Any], num: int) -> None:
    response = result["response"]
    assert isinstance(response, Response)

    data = response.json()
    assert data is not None

    if isinstance(data, dict):
        data = [data]

    assert isinstance(data, list)

    assert len(data) == int(num)
    for d in data:
        cand = KnownPulsar(**d)
        assert cand.id is not None
