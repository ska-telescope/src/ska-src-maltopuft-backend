"""Integration tests for the TokenManager class and SKA-IAM provider."""

# ruff: noqa: D103

from unittest import mock

import jwt
import pytest

from ska_src_maltopuft_backend.core.config import settings
from ska_src_maltopuft_backend.core.token_manager.token_manager import (
    TokenManager,
    get_token_manager,
)


@pytest.fixture(autouse=True)
def enable_auth(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setattr(
        settings,
        "AUTH_DISABLED",
        0,
    )


@pytest.fixture
def token_manager() -> TokenManager:
    return get_token_manager()


@pytest.mark.asyncio
async def test_get_client_token_is_success(
    token_manager: TokenManager,
) -> None:
    await token_manager.get_token()
    assert token_manager.token is not None


@pytest.mark.asyncio
async def test_client_token_has_maltopuft_audience(
    token_manager: TokenManager,
) -> None:
    await token_manager.get_token()
    assert token_manager.token is not None
    decoded_token = jwt.decode(
        token_manager.token,
        options={"verify_signature": False},
    )
    assert decoded_token.get("aud") == settings.AUTH.MALTOPUFT_IAM_CLIENT_NAME


@pytest.mark.asyncio
async def test_current_time_equals_expiry_time_returns_new_client_token(
    token_manager: TokenManager,
) -> None:
    with mock.patch("time.time", return_value=0):
        token = await token_manager.get_token()
    assert token is not None


@pytest.mark.asyncio
async def test_new_client_token_retrieved_on_expiry(
    token_manager: TokenManager,
) -> None:
    initial_expires_at = token_manager.token_expires_at
    initial_token = token_manager.token

    with mock.patch(
        "time.time",
        return_value=token_manager.token_expires_at + 1,
    ):
        await token_manager.get_token()

    # A new token should have been retrieved
    assert token_manager.token is not None
    assert token_manager.token != initial_token

    # Expiry time should have been updated
    assert token_manager.token_expires_at > initial_expires_at
