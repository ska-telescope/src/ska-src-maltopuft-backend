"""Database initialisation tests."""

# ruff: noqa: D103

import pytest
from pytest_bdd import given, scenarios, then, when
from sqlalchemy.orm import Session

from ska_src_maltopuft_backend.app.models import Entity
from ska_src_maltopuft_backend.core.config import settings
from ska_src_maltopuft_backend.core.database.init_db import deinit_db, init_db
from tests.mocks.factory import test_data_factory

scenarios("./init_db.feature")


@given("initial entities are configured")
def entity_configured(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setattr(
        settings,
        "MALTOPUFT_ENTITIES",
        [
            test_data_factory.entity(type="RFI"),
            test_data_factory.entity(type="SINGLE_PULSE"),
        ],
    )


@given("no initial entities are configured")
def no_entities_configured(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setattr(
        settings,
        "MALTOPUFT_ENTITIES",
        None,
    )


@when("the database is initialised")
def database_initialised(db: Session) -> None:
    init_db(db=db)


@when("the database is deinitialised")
def database_deinitialised(db: Session) -> None:
    deinit_db(db=db)


@then("the label entities should be created")
def check_label_entities_created(db: Session) -> None:
    entities = db.query(Entity).all()
    assert isinstance(settings.MALTOPUFT_ENTITIES, list)
    assert len(entities) == len(settings.MALTOPUFT_ENTITIES)


@then("there should be no label entities in the database")
def check_label_entities_removed(db: Session) -> None:
    entities = db.query(Entity).all()
    assert len(entities) == 0
